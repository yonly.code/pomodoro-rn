const ONE_MINUTE_IN_SECONDS = 60

export default class TimeParser {
    count = null;

    constructor(count) {
        this.count = count
    }

    getSeconds() {
        const value = this.count % ONE_MINUTE_IN_SECONDS
        if(value <= 9) return "0"+ value
        return value
    }

    getMinutes() {
        const value = Math.floor(this.count / ONE_MINUTE_IN_SECONDS)
        if(value <= 9) return "0"+ value
        return valu
    }


}