import { Button, Text, View, } from "react-native";
import { ContainerStyles } from "../styles/containers";
import { useState } from "react";
import TimeParser from "../utils/TimeParser";

export default function PomodoroView() {
    const [state, setState] = useState({
        interval: null,
        countInSeconds: 0
    })

    const handleStartTimer = () => {
        setState(prev => ({
            ...prev,
            interval: setInterval(() => {
                setState(prev => ({
                    ...prev,
                    countInSeconds: prev.countInSeconds + 1
                }))
            }, 1000)
        }))
    }

    const handlePauseTimer = () => {
        clearInterval(state.interval)
    }

    const handleResetTimer = () => {
        setState(prev => ({
            ...prev,
            countInSeconds: 0,
        }))
    }

    const Timer = new TimeParser(state.countInSeconds)

    const clock = `${Timer.getMinutes() === 0 ? "" : `${Timer.getMinutes()} : `}${Timer.getSeconds()}`

    return (
        <View style={ContainerStyles}>
            <Text>
                {clock}
            </Text>
            <View style={{display: "flex", flexDirection:"row", justifyContent: "space-between"}}> 
                <Button title="start" onPress={handleStartTimer}/>
                <Button title="pause" onPress={handlePauseTimer}/>
                <Button title="reset" onPress={handleResetTimer} />
            </View>

        </View>
    )
}