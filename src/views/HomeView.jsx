import { Button, Text, View } from "react-native";
import { ContainerStyles } from "../styles/containers";

export default function HomeView({ navigation }) {

    console.log(navigation)

    const handleGoToPomodoroView = () => {
        navigation.navigate('Pomodoro')
    }

    return (
        <View style={ContainerStyles}>
            <Text>Hello World</Text>
            <Button title="go_to_pomodoro_view" onPress={handleGoToPomodoroView}> Click Me </Button>
        </View>
    )
}