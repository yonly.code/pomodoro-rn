import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import HomeView from './src/views/HomeView';
import PomodoroView from './src/views/PomodoroView';
import RecapView from './src/views/RecapView';

const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={HomeView}/>
        <Stack.Screen name="Pomodoro" component={PomodoroView}/>
        <Stack.Screen name="Recap" component={RecapView}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}
